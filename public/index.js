document.addEventListener('DOMContentLoaded', () => {
  const handleChange = event => {
    const { on } = event.detail;
    lightBulb.setAttribute('on', on);
    switchButton.setAttribute('on', on);
    if (on === 'true') {
      document.body.classList.add('on');
    } else {
      document.body.classList.remove('on');
    }
  }

  const lightBulb = document.querySelector('light-bulb');
  const switchButton = document.querySelector('switch-button');

  lightBulb.addEventListener('change', handleChange);
  switchButton.addEventListener('change', handleChange);
});