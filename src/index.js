import { wrapReact, wrapNative, withStyles } from './utils';

import SwitchButtonStyles from './common/SwitchButton/style.scss';

import SwitchButton from './common/SwitchButton';
import LightBulb from './common/LightBulb';

wrapReact('switch-button', withStyles(SwitchButtonStyles, SwitchButton));
wrapNative('light-bulb', LightBulb);