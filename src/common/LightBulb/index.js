import classNames from 'classnames';
import styles from './style.css';

const template = document.createElement('template');
template.innerHTML = `
<style>${styles}</style>
<div class="LightBulb">
  <div class="bulb"></div>
  <div class="cord"></div>
</div>`;

export default class LightBulb extends HTMLElement {
  constructor() {
    super();

    this.attachShadow({ mode: 'open' });
    this.shadowRoot.appendChild(template.content.cloneNode(true));
    this.container = this.shadowRoot.querySelector('.LightBulb');

    this.bulb = this.shadowRoot.querySelector('.bulb');
    this.bulb.addEventListener('click', this.handleBulbClick);

    console.log('LightBulb constructed!');
  }

  static get observedAttributes() {
    return ['on'];
  }

  get on() {
    return this.getAttribute('on') === 'true';
  }

  set on(value) {
    this.setAttribute('on', value === 'true');
  }

  handleBulbClick = () => {
    const event = new CustomEvent('change', { detail: { on: `${!this.on}` } });
    this.dispatchEvent(event);
  };

  attributeChangedCallback(name) {
    if (name === 'on') {
      const { on } = this;
      this.container.setAttribute('class', classNames('LightBulb', { on }));
    }
  }
}
