import React from 'react';
import cx from 'classnames';

export default class SwitchButton extends React.Component {
  static defaultProps = {
    on: 'false',
  };

  constructor(props) {
    super(props);
    console.log('SwitchButton constructed!');
  }

  handleClick = () => {
    const { dispatchEvent } = this.props;
    if (dispatchEvent) {
      const event = new CustomEvent('change', { detail: { on: `${this.props.on !== 'true'}` } });
      dispatchEvent(event);
    }
  };

  render() {
    const { on } = this.props;
    return (
      <div className={cx('SwitchButton', { on: on === 'true' })}>
        <div className="screw"></div>
        <div className="switch" onClick={this.handleClick}></div>
        <div className="screw"></div>
      </div>
    );
  }
}
