import React from 'react';
import { DireflowComponent, withStyles as withDirectFlowStyles, EventConsumer } from 'direflow-component';

export function wrapReact(componentName, componentClass, options = { useShadow: false }) {
  const component = new DireflowComponent();
  options = options || {};
  options.properties = options.properties || componentClass.defaultProps;
  component.configure({ name: componentName, ...options });
  component.create(withEventContext(componentClass));
  window.component = component;
  console.log(`[React] ${componentName} is registered as web component!`);
}

export function wrapNative(componentName, componentClass) {
  try {
    customElements.define(componentName, componentClass);
    console.log(`[Native] ${componentName} is registered as web component!`);
  } catch (error) { }
}

export function withStyles(componentStyle, componentClass) {
  const styledComponent = withDirectFlowStyles(componentStyle)(componentClass);
  styledComponent.defaultProps = componentClass.defaultProps;
  return styledComponent;
}

export function withEventContext(ComponentClass) {
  class EventedComponent extends React.Component {
    render() {
      return (
        <EventConsumer>
          {dispatchEvent => <ComponentClass {...this.props} dispatchEvent={dispatchEvent} />}
        </EventConsumer>
      )
    }
  }

  EventedComponent.defaultProps = ComponentClass.defaultProps;
  return EventedComponent;
}
