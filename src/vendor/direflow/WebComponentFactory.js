"use strict";
var __extends = (this && this.__extends) || (function () {
  var extendStatics = function (d, b) {
    extendStatics = Object.setPrototypeOf ||
      ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
      function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
  };
  return function (d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
  };
})();
var __assign = (this && this.__assign) || function () {
  __assign = Object.assign || function (t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
      s = arguments[i];
      for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
        t[p] = s[p];
    }
    return t;
  };
  return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
  function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
  return new (P || (P = Promise))(function (resolve, reject) {
    function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
    function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
    function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
    step((generator = generator.apply(thisArg, _arguments || [])).next());
  });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
  var _ = { label: 0, sent: function () { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
  return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function () { return this; }), g;
  function verb(n) { return function (v) { return step([n, v]); }; }
  function step(op) {
    if (f) throw new TypeError("Generator is already executing.");
    while (_) try {
      if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
      if (y = 0, t) op = [op[0] & 2, t.value];
      switch (op[0]) {
        case 0: case 1: t = op; break;
        case 4: _.label++; return { value: op[1], done: false };
        case 5: _.label++; y = op[1]; op = [0]; continue;
        case 7: op = _.ops.pop(); _.trys.pop(); continue;
        default:
          if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
          if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
          if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
          if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
          if (t[2]) _.ops.pop();
          _.trys.pop(); continue;
      }
      op = body.call(thisArg, _);
    } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
    if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
  }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var react_dom_1 = __importDefault(require("react-dom"));
var proxyRoot_1 = __importDefault(require("./services/proxyRoot"));
var styledComponentsHandler_1 = __importDefault(require("./services/styledComponentsHandler"));
var externalSourceHandler_1 = __importDefault(require("./services/externalSourceHandler"));
var fontLoaderHandler_1 = __importDefault(require("./services/fontLoaderHandler"));
var polyfillHandler_1 = __importDefault(require("./services/polyfillHandler"));
var iconLoaderHandler_1 = __importDefault(require("./services/iconLoaderHandler"));
var EventContext_1 = require("./components/EventContext");
var WebComponentFactory = /** @class */ (function () {
  function WebComponentFactory(properties, component, shadowOption, plugins, connectCallback) {
    this.componentAttributes = {};
    this.componentProperties = properties;
    this.rootComponent = component;
    this.shadow = shadowOption;
    this.plugins = plugins;
    this.connectCallback = connectCallback;
    this.reflectPropertiesToAttributes();
  }
  /**
   * All properties with primitive values is added to attributes.
   */
  WebComponentFactory.prototype.reflectPropertiesToAttributes = function () {
    var _this = this;
    Object.entries(this.componentProperties).forEach(function (_a) {
      var key = _a[0], value = _a[1];
      if (typeof value !== 'number' && typeof value !== 'string' && typeof value !== 'boolean') {
        return;
      }
      _this.componentAttributes[key] = value;
    });
  };
  /**
   * Create new class that will serve as the Web Component.
   */
  WebComponentFactory.prototype.create = function () {
    return __awaiter(this, void 0, void 0, function () {
      var factory;
      return __generator(this, function (_a) {
        switch (_a.label) {
          case 0:
            factory = this;
            /**
             * Wait for Web Component polyfills to be included in the host application.
             * Polyfill scripts are loaded async.
             */
            return [4 /*yield*/, polyfillHandler_1.default({ usesShadow: !!factory.shadow }, this.plugins)];
          case 1:
            /**
             * Wait for Web Component polyfills to be included in the host application.
             * Polyfill scripts are loaded async.
             */
            _a.sent();
            return [2 /*return*/, /** @class */ (function (_super) {
              __extends(class_1, _super);
              function class_1() {
                var _this = _super.call(this) || this;
                /**
                 * Dispatch an event on behalf of the Web Component
                 */
                _this.eventDispatcher = function (event) {
                  _this.dispatchEvent(event);
                };
                _this.subscribeToProperties();
                return _this;
              }
              Object.defineProperty(class_1, "observedAttributes", {
                /**
                 * Observe attributes for changes.
                 * Part of the Web Component Standard.
                 */
                get: function () {
                  return Object.keys(factory.componentAttributes).map(function (k) { return k.toLowerCase(); });
                },
                enumerable: true,
                configurable: true
              });
              /**
               * Web Component gets mounted on the DOM.
               */
              class_1.prototype.connectedCallback = function () {
                this.preparePropertiesAndAttributes();
                this.preparePlugins();
                this.mountReactApp({ initial: true });
                factory.connectCallback(this);
              };
              /**
               * When an attribute is changed, this callback function is called.
               * @param name name of the attribute
               * @param oldValue value before change
               * @param newValue value after change
               */
              class_1.prototype.attributeChangedCallback = function (name, oldValue, newValue) {
                if (oldValue === newValue) {
                  return;
                }
                factory.componentProperties[name] = newValue;
                this.mountReactApp();
              };
              /**
               * When a property is changed, this callback function is called.
               * @param name name of the property
               * @param oldValue value before change
               * @param newValue value after change
               */
              class_1.prototype.propertyChangedCallback = function (name, oldValue, newValue) {
                if (oldValue === newValue) {
                  return;
                }
                factory.componentProperties[name] = newValue;
                this.mountReactApp();
              };
              /**
               * Web Component gets unmounted from the DOM.
               */
              class_1.prototype.disconnectedCallback = function () {
                react_dom_1.default.unmountComponentAtNode(this);
              };
              /**
               * Setup getters and setters for all properties.
               * Here we ensure that the 'propertyChangedCallback' will get invoked
               * when a property changes.
               */
              class_1.prototype.subscribeToProperties = function () {
                if (!factory.rootComponent) {
                  return;
                }
                var properties = Object.assign({}, factory.componentProperties);
                var self = this;
                for (var key in properties) {
                  if (self[key] != null) {
                    properties[key] = self[key];
                  }
                }
                var propertyMap = {};
                Object.keys(properties).forEach(function (key) {
                  var presetValue = self[key];
                  propertyMap[key] = {
                    configurable: true,
                    enumerable: true,
                    get: function () {
                      return presetValue || properties[key];
                    },
                    set: function (newValue) {
                      var oldValue = properties[key];
                      properties[key] = newValue;
                      self.propertyChangedCallback(key, oldValue, newValue);
                    },
                  };
                });
                Object.defineProperties(self, propertyMap);
              };
              /**
               * Prepare all properties and attributes
               */
              class_1.prototype.preparePropertiesAndAttributes = function () {
                var self = this;
                Object.keys(factory.componentProperties).forEach(function (key) {
                  if (self.getAttribute(key)) {
                    factory.componentProperties[key] = self.getAttribute(key);
                  }
                  else if (self[key] != null) {
                    factory.componentProperties[key] = self[key];
                  }
                });
              };
              /**
               * Fetch and prepare all plugins.
               */
              class_1.prototype.preparePlugins = function () {
                fontLoaderHandler_1.default(factory.plugins);
                iconLoaderHandler_1.default(this, factory.plugins);
                externalSourceHandler_1.default(this, factory.plugins);
                styledComponentsHandler_1.default(this, factory.plugins);
              };
              /**
               * Generate react props based on properties and attributes.
               */
              class_1.prototype.reactProps = function () {
                factory.reflectPropertiesToAttributes();
                return __assign({}, factory.componentProperties);
              };
              /**
               * Mount React App onto the Web Component
               */
              class_1.prototype.mountReactApp = function (options) {
                var _this = this;
                var _a;
                var application = this.application();
                if (!factory.shadow) {
                  react_dom_1.default.render(application, this);
                }
                else {
                  var currentChildren = void 0;
                  if ((_a = options) === null || _a === void 0 ? void 0 : _a.initial) {
                    currentChildren = Array.from(this.children).map(function (child) {
                      return child.cloneNode(true);
                    });
                  }
                  var root = proxyRoot_1.default(this);
                  react_dom_1.default.render(react_1.default.createElement(root.open, null, application), this);
                  if (currentChildren) {
                    currentChildren.forEach(function (child) { return _this.append(child); });
                  }
                }
              };
              /**
               * Create the React App
               */
              class_1.prototype.application = function () {
                if (this._application) {
                  return this._application;
                }
                var baseApplication = (react_1.default.createElement(EventContext_1.EventProvider, { value: this.eventDispatcher }, react_1.default.createElement(factory.rootComponent, this.reactProps())));
                return baseApplication;
              };
              return class_1;
            }(HTMLElement))];
        }
      });
    });
  };
  return WebComponentFactory;
}());
exports.default = WebComponentFactory;
