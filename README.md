> This component was bootstrapped with [Direflow](https://direflow.io).

# Get Started

### 1. Install all dependencies

`yarn`

### 2. Patch the direflow node_modules

* Copy `src/vendor/direflow/WebComponentFactory.js`
* Paste into `node_modules/direflow-component/dist/`

### 3. Run the app

`yarn start`

### 2. Build the components

`yarn build`

# Components

## LightBulb
A component for displaying a light bulb.
```html
<light-bulb on="true|false"></light-bulb>
```

Supports the `change` event.
```javascript
document.querySelector('light-bulb').addEventListener('change', event => {
  const { on } = event.detail;
  console.log(on);
});
```

## SwitchButton
A component for displaying a light bulb.
```html
<switch-button on="true|false"></switch-button>
```

Supports the `change` event.
```javascript
document.querySelector('switch-button').addEventListener('change', event => {
  const { on } = event.detail;
  console.log(on);
});
```